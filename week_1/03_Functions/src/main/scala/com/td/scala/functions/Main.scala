package com.td.scala.functions

object Main extends App {

  println("Prepare characteristic sheets (technical) for all green car")

  println("Prepare characteristic sheets (showroom) for all pink car")

  println("""Build a concept car that embeded a turbo that make it the faster car in the world
      |when the pilot speed up, its speed rise up by 10 times than standard car
      |""".stripMargin)

  println("""Build a standard car that embeded a turbo that make it the luxurious car in the world
      |when the driver speed up, its speed rise up by 2 times
      |""".stripMargin)

}
