package com.td.scala.functions

object CarUtilities {

  /** Price mustn't appear, only color, nbDoor, nbPlaces* */
  val technicalFormat: Car => String = ???

  /** All caracteristics must me displayed* */
  val showRoomFormat: Car => String = ???

  /** Multiply parameter by 10* */
  val maxiTurbo: Int => Int = ???

  /** Multiply parameter by 2* */
  val softTurbo: Int => Int = ???

}
